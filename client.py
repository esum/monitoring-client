import json
import socket
import ssl
import time

import monitoring

import config


if __name__ == '__main__':
    context = ssl.create_default_context(ssl.Purpose.SERVER_AUTH, cafile=config.SERVER_CERT)
    context.load_cert_chain(certfile=config.CLIENT_CERT, keyfile=config.CLIENT_KEY)

    sock = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
    conn = context.wrap_socket(sock, server_side=False, server_hostname=config.SERVER_HOSTNAME)
    conn.connect((config.SERVER_ADDR, config.SERVER_PORT))
    conn.sendall(json.dumps({
        'loadavg': monitoring.loadavg(),
        'meminfo': monitoring.meminfo(),
        'uname': monitoring.uname(),
        'uptime': monitoring.uptime(),
        'systemd': monitoring.systemd(),
        'disk': monitoring.disk(),
        'netstats': monitoring.netstats(),
        'nvgpu': monitoring.nvgpu(),
    }).encode('ascii'))
    conn.sendall(b'\n')
    conn.recv(1)
    sock.close()
