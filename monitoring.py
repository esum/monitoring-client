import os
import shutil

try:
	import dbus
except ImportError:
	dbus = None
try:
	from nvml import Nvml
except ImportError:
	Nvml = None


def loadavg():
	with open('/proc/loadavg') as loadavg_file:
		loadavg = loadavg_file.read()
	loadavg = [ float(load) for load in loadavg.split(maxsplit=3)[:3] ]
	return {
		'1': loadavg[0],
		'5': loadavg[1],
		'15': loadavg[2],
	}

def meminfo():
	with open('/proc/meminfo') as meminfo_file:
		meminfo = meminfo_file.read()
	meminfo = [ info.split() for info in meminfo.strip().split('\n') ]
	return { info[0][:-1]: int(info[1]) for info in meminfo }

def uname():
	uname = os.uname()
	return {
		'sysname': uname.sysname,
		'nodename': uname.nodename,
		'release': uname.release,
		'version': uname.version,
		'machine': uname.machine,
	}

def uptime():
	with open('/proc/uptime') as uptime_file:
		uptime = uptime_file.read()
	uptime = uptime.strip().split()
	uptime = [ float(time) for time in uptime ]
	return {
		'uptime': uptime[0],
		'idle': uptime[1],
	}

def systemd():
	if dbus is None:
		return []
	bus = dbus.SystemBus()
	systemd = bus.get_object('org.freedesktop.systemd1', '/org/freedesktop/systemd1')
	manager = dbus.Interface(systemd, 'org.freedesktop.systemd1.Manager')
	return [ {
		'unit': str(unit[0]),
		'description': str(unit[1]),
		'load': str(unit[2]),
		'active': str(unit[3]),
		'sub': str(unit[4]),
	} for unit in manager.ListUnits() ]

def disk():
	with open('/proc/mounts') as mounts_file:
		mounts = mounts_file.readlines()
	mounts = [ mount.split() for mount in mounts ]
	ret = []
	for mount in mounts:
		if mount[2] not in {'ext4', 'vfat', 'nfs4'}:
			continue
		total, used, free = shutil.disk_usage(mount[1])
		ret.append({
			'dev': mount[0],
			'mount': mount[1],
			'total': total,
			'used': used,
			'free': free,
		})
	return ret

def netstats():
	stats = {}
	for iface in os.listdir('/sys/class/net'):
		stats[iface] = {}
		for stat in os.listdir(f'/sys/class/net/{iface}/statistics'):
			with open(f'/sys/class/net/{iface}/statistics/{stat}') as stat_file:
				stats[iface][stat] = int(stat_file.read())
	return stats

def nvgpu():
	if Nvml is None:
		return []
	nvgpu = []
	with Nvml() as nvml:
		for gpu in nvml:
			nvgpu.append({
				'name': gpu.name,
				'uuid': gpu.uuid,
				'fan': gpu.fan_speed,
				'temp': gpu.temperature,
				'perf': gpu.performance_state.value,
				'power': gpu.power_usage,
				'mem': gpu.memory_info.used,
				'util': gpu.utilization_rates.gpu,
			})
	return nvgpu
